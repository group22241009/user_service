package storage

import (
	"context"
	pbu "user_service/genproto/user_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	Client() IClientStorage
	Courier() ICourierStorage
	User() IUserStorage
}

type IUserStorage interface {
	Create(context.Context, *pbu.CreateUser) (*pbu.User, error)
	Get(context.Context, *pbu.PrimaryKeyUser) (*pbu.User, error)
	GetList(context.Context, *pbu.GetListRequestUser) (*pbu.UserListResponse, error)
	Update(context.Context, *pbu.User) (*pbu.User, error)
	Patch(context.Context,*pbu.UpdatePasswordUser)(*emptypb.Empty, error)
	Delete(context.Context, *pbu.PrimaryKeyUser) (*emptypb.Empty, error)
}
type IBranchStorage interface {
	Create(context.Context, *pbu.CreateBranch) (*pbu.Branch, error)
	Get(context.Context, *pbu.PrimaryKeyBranch) (*pbu.Branch, error)
	GetList(context.Context, *pbu.GetListRequestBranch) (*pbu.BranchListResponse, error)
	Update(context.Context, *pbu.Branch) (*pbu.Branch, error)
	Delete(context.Context, *pbu.PrimaryKeyBranch) (*emptypb.Empty, error)
}
type IClientStorage interface {
	Create(context.Context, *pbu.CreateClient) (*pbu.Client, error)
	Get(context.Context, *pbu.PrimaryKeyClient) (*pbu.Client, error)
	GetList(context.Context, *pbu.GetListRequestClient) (*pbu.ClientListResponse, error)
	Update(context.Context, *pbu.Client) (*pbu.Client, error)
	Patch(context.Context,*pbu.UpdatePasswordClient)(*emptypb.Empty, error)
	Delete(context.Context, *pbu.PrimaryKeyClient) (*emptypb.Empty, error)
	GetByCredentials(context.Context, *pbu.ClientLoginRequest) (*pbu.Client, error)
}
type ICourierStorage interface {
	Create(context.Context, *pbu.CreateCourier) (*pbu.Courier, error)
	Get(context.Context, *pbu.PrimaryKeyCourier) (*pbu.Courier, error)
	GetList(context.Context, *pbu.GetListRequestCourier) (*pbu.CourierListResponse, error)
	Update(context.Context, *pbu.Courier) (*pbu.Courier, error)
	Patch(context.Context,*pbu.UpdatePasswordCourier)(*emptypb.Empty, error)
	Delete(context.Context, *pbu.PrimaryKeyCourier) (*emptypb.Empty, error)
}

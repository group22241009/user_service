package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"user_service/pkg/helper"
	"user_service/pkg/logger"

	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"

	pbu "user_service/genproto/user_service"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchStorage {
	return &branchRepo{
		db:  db,
		log: log,
	}
}
func (u *branchRepo) Create(ctx context.Context, request *pbu.CreateBranch) (*pbu.Branch, error) {

	branch := pbu.Branch{}

	query := `insert into branches(id, name, phone, delivery_tarif, start_hour, end_hour, address, destination, active) values ($1, $2, $3, $4, to_timestamp($5, 'HH24:MI:SS'), to_timestamp($6, 'HH24:MI:SS'), $7, $8, $9) returning id, name, phone, delivery_tarif, start_hour::varchar, end_hour::varchar, address, destination, active`

	request.DeliveryTarif = uuid.NewString()
	if err := u.db.QueryRow(ctx, query, uuid.New().String(), request.GetName(), request.GetPhone(), request.DeliveryTarif, request.GetStartHour(), request.GetEndHour(), request.GetAddress(), request.GetDestination(), request.GetActive()).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTarif,
		&branch.StartHour,
		&branch.EndHour,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
	); err != nil {
		fmt.Println("errr", err)
	}
	return &branch, nil
}
func (u *branchRepo) Get(ctx context.Context, request *pbu.PrimaryKeyBranch) (*pbu.Branch, error) {
	branch := pbu.Branch{}
	var delivery_tarif sql.NullString
	query := ` select id,name,phone,delivery_tarif,start_hour::varchar,end_hour::varchar,address,destination,active from branches where deleted_at=0 and id=$1 `
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&delivery_tarif,
		&branch.StartHour,
		&branch.EndHour,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
	); err != nil {
		fmt.Println("Err",err)
	}
	if delivery_tarif.Valid {
		branch.DeliveryTarif = delivery_tarif.String
	}

	return &branch, nil
}
func (u *branchRepo) GetList(ctx context.Context, request *pbu.GetListRequestBranch) (*pbu.BranchListResponse, error) {
	var (
		resp   = pbu.BranchListResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	countQuery := `select count(*) from branches` + filter
	// if request.Search != "" {
	// 	filter += fmt.Sprintf(` and name ilike '%s'`, request.GetSearch())
	// }
	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("err", err)
	}

	query := `select id,name,phone,delivery_tarif,start_hour::varchar,end_hour::varchar,address,destination,active from branches` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	// if request.Search != "" {
	// 	query += fmt.Sprintf(` and name ilike '%s'`, request.GetSearch())
	// }

	// var delivery_tarif sql.NullString

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		fmt.Println("err", err)
	}

	defer rows.Close()

	for rows.Next() {
		branch := pbu.Branch{}

		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTarif,
			&branch.StartHour,
			&branch.EndHour,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
		); err != nil {
			fmt.Println("Error while getting ", err)

		}
		// if delivery_tarif.Valid {
		// 	branch.DeliveryTarif = delivery_tarif.String
		// }

		resp.Branchs = append(resp.Branchs, &branch)
	}
	resp.Count = count

	return &resp, nil
}
func (u *branchRepo) Update(ctx context.Context, request *pbu.Branch) (*pbu.Branch, error) {
	var (
		branch = pbu.Branch{}
		params = make(map[string]interface{})
		query  = ` update branches set  `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("branch id", request.GetId())

	if request.GetAddress() != "" {
		params["address"] = request.Address
		filter += " address= @address, "
	}
	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name, "
	}
	if request.GetDeliveryTarif() != "" {
		params["delivery_tarif"] = request.GetDeliveryTarif()
		filter += " delivery_tarif= @delivery_tarif,"
	}
	if request.GetDestination() != "" {
		params["destination"] = request.GetDestination()
		filter += " destination= @destination, "
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += "phone= @phone, "
	}
	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active, "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, phone, delivery_tarif, start_hour::varchar, end_hour::varchar, address, destination, active  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTarif,
		&branch.StartHour,
		&branch.EndHour,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
	); err != nil {
		fmt.Println("error querying", err)
	}

	return &branch, nil

}

func (u *branchRepo) Delete(ctx context.Context, request *pbu.PrimaryKeyBranch) (*emptypb.Empty, error) {
	query := `update branches set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)

	return nil, err
}

package postgres

import (
	"context"
	"fmt"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
	"user_service/pkg/security"

	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"

	pbu "user_service/genproto/user_service"
)

type clientRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewClientRepo(db *pgxpool.Pool, log logger.ILogger) storage.IClientStorage {
	return &clientRepo{
		db:  db,
		log: log,
	}
}

func (u *clientRepo) Create(ctx context.Context, request *pbu.CreateClient) (*pbu.Client, error) {

	var (
		client = pbu.Client{}
		err    error
	)

	hashedPassword, err := security.HashPassword(request.Password)
	if err !=nil{
		return nil, err
	}
	query := ` insert into clients(id,first_name,last_name,phone,active,login,password,date_of_birth,last_ordered_date,total_orders_sum,total_orders_count,discount_type,discount_amount) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)
	returning id,first_name,last_name,phone,active,login,password,date_of_birth::varchar,last_ordered_date::varchar,total_orders_sum,total_orders_count,discount_type,discount_amount`
	if err = u.db.QueryRow(ctx, query, uuid.New().String(), request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetActive(), request.GetLogin(), hashedPassword, request.GetDateOfBirth(), request.GetLastOrderedDate(), request.GetTotalOrdersSum(), request.GetTotalOrdersCount(), request.GetDiscountType(), request.GetDiscountAmount()).Scan(
		&client.Id,        //1
		&client.FirstName, //2
		&client.LastName,  //3
		&client.Phone,     //4
		&client.Active,    //5
		&client.Login,
		&client.Password,
		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
	); err != nil {
		fmt.Println("error querying", err)
	}

	return &client, nil
}
func (u *clientRepo) Get(ctx context.Context, request *pbu.PrimaryKeyClient) (*pbu.Client, error) {

	client := pbu.Client{}
	query := ` select id,first_name,last_name,phone,active,login,password,date_of_birth::varchar,last_ordered_date::varchar,total_orders_sum,total_orders_count,discount_type,discount_amount from clients where deleted_at=0 and id=$1 `
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Active,
		&client.Login,
		&client.Password,
		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
	); err != nil {
		u.log.Error("error", logger.Error(err))
		return nil, err
	}
	return &client, nil
}
func (u *clientRepo) GetList(ctx context.Context, request *pbu.GetListRequestClient) (*pbu.ClientListResponse, error) {
	var (
		resp   = pbu.ClientListResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	countQuery := `select count(*) from clients ` + filter
	if request.Search != "" {
		countQuery += fmt.Sprintf(` and last_name ilike '%s'`, request.GetSearch())
	}
	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := ` select id,first_name,last_name,phone,active,login,password,date_of_birth::varchar,last_ordered_date::varchar,total_orders_sum,total_orders_count,discount_type,discount_amount from clients `
	if request.Search != "" {
		query += fmt.Sprintf(` and last_name ilike '%s'`, request.GetSearch())
	}
	query += filter + fmt.Sprintf(" offset %d limit %d", offset, request.GetLimit())
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error while getting ", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		client := pbu.Client{}
		if err = rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Active,
			&client.Login,
			&client.Password,
			&client.DateOfBirth,
			&client.LastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&client.DiscountType,
			&client.DiscountAmount,
		); err != nil {
			u.log.Error("err", logger.Error(err))
			return nil, err
		}
		resp.Clients = append(resp.Clients, &client)
	}
	resp.Count = count
	return &resp, nil
}
func (u *clientRepo) Update(ctx context.Context, request *pbu.Client) (*pbu.Client, error) {
	var (
		client = pbu.Client{}
		params = make(map[string]interface{})
		query  = `update clients set `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("client id", request.GetId())

	if request.GetFirstName() != "" {
		params["first_name"] = request.GetFirstName()
		filter += " first_name= @first_name, "
	}
	if request.GetLastName() != "" {
		params["last_name"] = request.GetLastName()
		filter += " last_name= @last_name, "
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += " phone= @phone,"
	}
	if request.GetDateOfBirth() != "" {
		params["date_of_birth"] = request.GetDateOfBirth()
		filter += " date_of_birth= @date_of_birth,"
	}
	if request.GetDiscountAmount() != 0.0 {
		params["discount_amount"] = request.GetDiscountAmount()
		filter += " discount_amount= @discount_amount, "
	}
	if request.GetLastOrderedDate() != "" {
		params["last_ordered_date"] = request.GetLastOrderedDate()
		filter += " last_ordered_date= @last_ordered_date,"
	}
	if request.GetTotalOrdersCount() != 0.0 {
		params["total_ordered_count"] = request.GetTotalOrdersCount()
		filter += " total_orders_count= @total_ordered_count, "
	}
	if request.GetTotalOrdersSum() != 0.0 {
		params["total_ordered_sum"] = request.GetTotalOrdersSum()
		filter += " total_orders_sum= @total_ordered_sum, "
	}

	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active, "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,first_name,last_name,phone,active,login,password,date_of_birth::varchar,last_ordered_date::varchar,total_orders_sum,total_orders_count,discount_type,discount_amount `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Active,
		&client.Login,
		&client.Password,
		&client.DateOfBirth,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
	); err != nil {
		fmt.Println("err", err)
	}
	return &client, nil
}
func (u *clientRepo) Patch(ctx context.Context, request *pbu.UpdatePasswordClient) (*emptypb.Empty, error) {
	query := `update clients set password =$1 where login =$2`
	if _, err := u.db.Exec(ctx, query, request.Password, request.Login); err != nil {
		fmt.Println("error in service layer to update password", err)
	}
	return nil, nil
}
func (u *clientRepo) Delete(ctx context.Context, request *pbu.PrimaryKeyClient) (*emptypb.Empty, error) {
	query := `update clients set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)

	return nil, err
}
func (u *clientRepo) GetByCredentials(ctx context.Context, req *pbu.ClientLoginRequest) (*pbu.Client, error) {
	client := pbu.Client{}

	if err := u.db.QueryRow(ctx, `select password, login  from clients where login = $1`, req.GetLogin()).Scan(
		&client.Password,
		&client.Login,
	); err != nil {
		fmt.Println("error is credentials", err)
	}

	return &client, nil
}

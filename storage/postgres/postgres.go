package postgres

import (
	"context"
	"fmt"
	"strings"
	"user_service/config"
	"user_service/pkg/logger"
	"user_service/storage"

	"github.com/golang-migrate/migrate"
  _"github.com/golang-migrate/migrate/database/postgres"
  _"github.com/golang-migrate/migrate/source/file"
  _ "github.com/golang-migrate/migrate/v4" // migrate package is imported for use
  _ "github.com/golang-migrate/migrate/v4/database" // database is needed for migration
  _ "github.com/golang-migrate/migrate/v4/database/postgres" // postgres is used for database
  _ "github.com/golang-migrate/migrate/v4/source/file" // file is needed for migration URL
  "github.com/jackc/pgx/v5/pgxpool"
  _ "github.com/lib/pq" // pq is imported for PostgreSQL driver

)

type Store struct {
	db  *pgxpool.Pool
	cfg config.Config
	log logger.ILogger
}

func New(ctx context.Context, cfg config.Config, log logger.ILogger) (storage.IStorage, error) {
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return Store{}, err
	}

	//migration
	m, err := migrate.New("file://migration/postgres", url)
	if err != nil {
		return Store{}, err
	}

	if err = m.Up(); err != nil {
		fmt.Println("*************")
		if !strings.Contains(err.Error(), "no change") {
			fmt.Println("entered", err)
			fmt.Println("-----------")
			version, dirty, err := m.Version()
			if err != nil {
				return Store{}, err
			}

			if dirty {
				version--
				if err = m.Force(int(version)); err != nil {
					return Store{}, err
				}
			}
			fmt.Println("*************")
			return Store{}, err
		}
	}

	return Store{
		db:  pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close() {
	s.db.Close()
}
func (s Store) User() storage.IUserStorage {
	return NewUserRepo(s.db, s.log)
}
func (s Store) Branch() storage.IBranchStorage {
	return NewBranchRepo(s.db, s.log)
}
func (s Store) Courier() storage.ICourierStorage {
	return NewCourierRepo(s.db, s.log)
}
func (s Store) Client() storage.IClientStorage {
	return NewClientRepo(s.db, s.log)
}

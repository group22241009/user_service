package postgres

import (
	"context"
	"fmt"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
	"user_service/storage"

	pbu "user_service/genproto/user_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type userRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewUserRepo(db *pgxpool.Pool, log logger.ILogger) storage.IUserStorage {
	return &userRepo{
		db:  db,
		log: log,
	}
}
func (u *userRepo) Create(ctx context.Context, request *pbu.CreateUser) (*pbu.User, error) {

	var (
		user = pbu.User{}
		err  error
	)
	query := ` insert into users(id,first_name,last_name,phone,active,login,password,searching_column) values($1,$2,$3,$4,$5.$6,$7)
	returning id,first_name,last_name,phone,active,login,password,`

	searchingColumn := fmt.Sprintf("%s %s", request.GetLastName(), request.GetFirstName())
	if err = u.db.QueryRow(ctx, query, uuid.New().String(), request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetLogin(), request.GetPassword(), searchingColumn).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.Active,
		&user.Login,
		&user.Password,
	); err != nil {
		u.log.Error("error", logger.Error(err))
		return nil, err
	}

	return &user, nil

}
func (u *userRepo) Get(ctx context.Context, request *pbu.PrimaryKeyUser) (*pbu.User, error) {

	user := pbu.User{}
	query := ` select id,first_name,last_name,phone,active,login,password from users where deleted_at=0 and id=$1 `
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.Active,
		&user.Login,
		&user.Password,
	); err != nil {
		u.log.Error("error", logger.Error(err))
		return nil, err
	}
	return &user, nil
}
func (u *userRepo) GetList(ctx context.Context, request *pbu.GetListRequestUser) (*pbu.UserListResponse, error) {
	var (
		resp   = pbu.UserListResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from users ` + filter
	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := ` select id,first_name,last_name,phone,active,login,password from users ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error while getting ", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		user := pbu.User{}
		if err = rows.Scan(
			&user.Id,
			&user.FirstName,
			&user.LastName,
			&user.Phone,
			&user.Active,
			&user.Login,
			&user.Password,
		); err != nil {
			u.log.Error("err", logger.Error(err))
			return nil, err
		}
		resp.Users = append(resp.Users, &user)
	}
	resp.Count = count

	return &resp, nil

}
func (u *userRepo) Update(ctx context.Context, request *pbu.User) (*pbu.User, error) {
	var (
		user   = pbu.User{}
		params = make(map[string]interface{})
		query  = `update couriers set `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("user id", request.GetId())

	if request.GetFirstName() != "" {
		params["first_name"] = request.GetFirstName()
		filter += " first_name=@first_name"
	}
	if request.GetLastName() != "" {
		params["last_name"] = request.GetLastName()
		filter += " lastt_name=@last_name"
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += " phone=@phone"
	}
	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active"
	}
	
	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,first_name,last_name,phone,active,login,password  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.Active,
		&user.Login,
		&user.Password,
	); err != nil {
		u.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &user, nil

}
func (u *userRepo) Patch(ctx context.Context, request *pbu.UpdatePasswordUser) (*emptypb.Empty, error) {
	query := ` update users set password =$1 where id=$2`
	_, err := u.db.Exec(ctx, query, request.Password, request.Id)

	return nil, err
}
func (u *userRepo) Delete(ctx context.Context, request *pbu.PrimaryKeyUser) (*emptypb.Empty, error) {
	query := `update users set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)

	return nil, err
}

package postgres

import (
	"context"
	"fmt"
	"user_service/pkg/helper"
	"user_service/pkg/logger"

	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"

	pbu "user_service/genproto/user_service"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICourierStorage {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (u *courierRepo) Create(ctx context.Context, request *pbu.CreateCourier) (*pbu.Courier, error) {
	var (
		courier = pbu.Courier{}
		err     error
	)
	query := ` insert into couriers(id,first_name,last_name,phone,active,login,password,max_order_count,branch_id,searching_column) values($1,$2,$3,$4,$5.$6,$7,$8,$9)
	returning id,first_name,last_name,phone,active,login,password,max_order_count,branch_id`

	request.BranchId = uuid.New().String()
	searchingColumn := fmt.Sprintf("%s %s %s", request.GetLastName(), request.GetFirstName(), request.GetBranchId())
	if err = u.db.QueryRow(ctx, query, uuid.New().String(), request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetLogin(), request.GetPassword(), request.GetMaxOrderCount(), request.GetBranchId(), searchingColumn).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.Login,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
	); err != nil {
		u.log.Error("error", logger.Error(err))
		return nil, err
	}

	return &courier, nil

}
func (u *courierRepo) Get(ctx context.Context, request *pbu.PrimaryKeyCourier) (*pbu.Courier, error) {
	courier := pbu.Courier{}
	query := ` select id,first_name,last_name,phone,active,login,password,max_order_count,branch_id from couriers where deleted_at=0 and id=$1 `
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.Login,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
	); err != nil {
		u.log.Error("error", logger.Error(err))
		return nil, err
	}
	return &courier, nil
}
func (u *courierRepo) GetList(ctx context.Context, request *pbu.GetListRequestCourier) (*pbu.CourierListResponse, error) {
	var (
		resp   = pbu.CourierListResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from couriers ` + filter
	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := ` select id,first_name,last_name,phone,active,login,password,max_order_count,branch_id from couriers ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error while getting ", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		courier := pbu.Courier{}
		if err = rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Active,
			&courier.Login,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
		); err != nil {
			u.log.Error("err", logger.Error(err))
			return nil, err
		}
		resp.Couriers = append(resp.Couriers, &courier)
	}
	resp.Count = count

	return &resp, nil

}
func (u *courierRepo) Update(ctx context.Context, request *pbu.Courier) (*pbu.Courier, error) {
	var (
		courier = pbu.Courier{}
		params  = make(map[string]interface{})
		query   = `update couriers set `
		filter  = ""
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetFirstName() != "" {
		params["first_name"] = request.GetFirstName()
		filter += " first_name=@first_name"
	}
	if request.GetLastName() != "" {
		params["last_name"] = request.GetLastName()
		filter += " lastt_name=@last_name"
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += " phone=@phone"
	}
	if request.GetMaxOrderCount() != 0 {
		params["max_order_count"] = request.GetMaxOrderCount()
		filter += " max_order_count=@max_order_count"
	}
	if request.GetBranchId() != "" {
		params["branch_id"] = request.GetBranchId()
		filter += " branch_id=@branch_id"
	}
	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active"
	}
	
	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,first_name,last_name,phone,active,login,password,max_order_count,branch_id  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.Login,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
	); err != nil {
		u.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &courier, nil
}
func (u *courierRepo) Patch(ctx context.Context, request *pbu.UpdatePasswordCourier) (*emptypb.Empty, error) {
	query := ` update couriers set password =$1 where id=$2`
	_, err := u.db.Exec(ctx, query, request.Password, request.Id)

	return nil, err
}
func (u *courierRepo) Delete(ctx context.Context, request *pbu.PrimaryKeyCourier) (*emptypb.Empty, error) {
	query := `update couriers set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)

	return nil, err
}

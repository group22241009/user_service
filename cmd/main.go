package main

import (
	"context"
	"net"
	"user_service/config"
	"user_service/pkg/logger"
	"user_service/storage/postgres"

	"github.com/gin-gonic/gin"
	"user_service/grpc/client"
	"user_service/grpc"
	
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postgres.New(context.Background(), cfg, log)
	if err != nil {
		log.Error("error while connection to postgres", logger.Error(err))
		return
	}
	defer pgStore.Close()

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Error("error while dialing grpc clients", logger.Error(err))
		return
	}

	grpcServer :=grpc.SetUpServer(pgStore, services, log)

	lis, err := net.Listen("tcp","localhost:8003")
	if err != nil {
		log.Error("error while listening grpc host port", logger.Error(err))
		return
	}

	log.Info("Service is running...", logger.Any("grpc port", cfg.ServiceGrpcPort))
	if err = grpcServer.Serve(lis); err != nil {
		log.Error("error while listening grpc", logger.Error(err))
	}
}

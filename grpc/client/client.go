package client

import (
	"user_service/genproto/user_service"

	"user_service/config"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	BranchService() user_service.BranchServiceClient
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient
	UserService() user_service.UserServiceClient
}

type grpcClients struct {
	branchService  user_service.BranchServiceClient
	clientService  user_service.ClientServiceClient
	courierService user_service.CourierServiceClient
	userService    user_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connUserService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		branchService:  user_service.NewBranchServiceClient(connUserService),
		clientService:  user_service.NewClientServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),
		userService:    user_service.NewUserServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}
func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}
func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}
func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

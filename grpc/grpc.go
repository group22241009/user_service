package grpc

import (
	pb "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/service"
	"user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pb.RegisterBranchServiceServer(grpcServer, service.NewBranchService(strg, services, log))
	pb.RegisterClientServiceServer(grpcServer, service.NewClientService(strg, services, log))
	pb.RegisterCourierServiceServer(grpcServer, service.NewCourierService(strg, services, log))
	pb.RegisterUserServiceServer(grpcServer, service.NewUserService(strg, services, log))
	reflection.Register(grpcServer)

	return grpcServer
}

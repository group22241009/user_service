CREATE TYPE "discount_type" AS ENUM (
  'sum',
  'percent'
);


CREATE TABLE "branches" (
  "id" uuid PRIMARY KEY,
  "name" varchar(20),
  "phone" varchar(15),
  "delivery_tarif" uuid,
  "start_hour" timestamp without time zone,
  "end_hour" timestamp without time zone,
  "address" varchar(20),
  "destination" varchar(20),
  "active" bool DEFAULT true,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "users" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar(15),
  "last_name" varchar(15),
  "phone" varchar(15),
  "active" bool DEFAULT true,
  "login" varchar(10),
  "password" varchar(10),
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "couriers" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar(15),
  "last_name" varchar(15),
  "phone" varchar(15),
  "active" bool DEFAULT true,
  "login" varchar(10),
  "password" varchar(10),
  "max_order_count" int,
  "branch_id" uuid,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" int DEFAULT 0
);

CREATE TABLE "clients" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar(15),
  "last_name" varchar(15),
  "phone" varchar(15),
  "active" bool DEFAULT true,
  "login" varchar(10),
  "password" varchar(10),
  "date_of_birth" date,
  "last_ordered_date" date,
  "total_orders_sum" float,
  "total_orders_count" int,
  "discount_type" discount_type,
  "discount_amount" float,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" int DEFAULT 0
);
ALTER TABLE "couriers" ADD FOREIGN KEY ("branch_id") REFERENCES "branches" ("id");

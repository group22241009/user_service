package service

import (
	"context"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"

	pbo "user_service/genproto/user_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCourierService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *courierService {
	return &courierService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *courierService) Create(ctx context.Context, request *pbo.CreateCourier) (*pbo.Courier, error) {
	return o.storage.Courier().Create(ctx, request)
}
func (o *courierService) Get(ctx context.Context, request *pbo.PrimaryKeyCourier) (*pbo.Courier, error) {
	return o.storage.Courier().Get(ctx, request)
}
func (o *courierService) GetList(ctx context.Context, request *pbo.GetListRequestCourier) (*pbo.CourierListResponse, error) {
	return o.storage.Courier().GetList(ctx, request)
}
func (o *courierService) Update(ctx context.Context, request *pbo.Courier) (*pbo.Courier, error) {
	return o.storage.Courier().Update(ctx, request)
}
func (o *courierService) Patch(ctx context.Context,request *pbo.UpdatePasswordCourier)(*emptypb.Empty, error)  {
	return o.storage.Courier().Patch(ctx,request)
}
func (o *courierService) Delete(ctx context.Context, request *pbo.PrimaryKeyCourier) (*emptypb.Empty, error) {
	return o.storage.Courier().Delete(ctx, request)
}

package service

import (
	"context"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
	pbo "user_service/genproto/user_service"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewBranchService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *branchService {
	return &branchService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *branchService) Create(ctx context.Context, request *pbo.CreateBranch) (*pbo.Branch, error) {
	return o.storage.Branch().Create(ctx,request)
}
func (o *branchService) Get(ctx context.Context, request *pbo.PrimaryKeyBranch) (*pbo.Branch, error) {
	return o.storage.Branch().Get(ctx, request)
}
func (o *branchService) GetList(ctx context.Context, request *pbo.GetListRequestBranch) (*pbo.BranchListResponse, error) {
	return o.storage.Branch().GetList(ctx, request)
}
func (o *branchService) Update(ctx context.Context, request *pbo.Branch) (*pbo.Branch, error) {
	return o.storage.Branch().Update(ctx, request)
}
func (o *branchService) Delete(ctx context.Context, request *pbo.PrimaryKeyBranch) (*emptypb.Empty, error) {
	return o.storage.Branch().Delete(ctx, request)
}

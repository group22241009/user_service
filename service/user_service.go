package service

import (
	"context"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"

	pbo "user_service/genproto/user_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type userService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewUserService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *userService {
	return &userService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *userService) Create(ctx context.Context, request *pbo.CreateUser) (*pbo.User, error) {
	return o.storage.User().Create(ctx, request)
}
func (o *userService) Get(ctx context.Context, request *pbo.PrimaryKeyUser) (*pbo.User, error) {
	return o.storage.User().Get(ctx, request)
}
func (o *userService) GetList(ctx context.Context, request *pbo.GetListRequestUser) (*pbo.UserListResponse, error) {
	return o.storage.User().GetList(ctx, request)
}
func (o *userService) Update(ctx context.Context, request *pbo.User) (*pbo.User, error) {
	return o.storage.User().Update(ctx, request)
}
func (o *userService) Patch(ctx context.Context, request *pbo.UpdatePasswordUser) (*emptypb.Empty, error) {
	return o.storage.User().Patch(ctx, request)
}
func (o *userService) Delete(ctx context.Context, request *pbo.PrimaryKeyUser) (*emptypb.Empty, error) {
	return o.storage.User().Delete(ctx, request)
}

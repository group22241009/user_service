package service

import (
	"context"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"

	pbo "user_service/genproto/user_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type clientService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewClientService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *clientService {
	return &clientService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *clientService) Create(ctx context.Context, request *pbo.CreateClient) (*pbo.Client, error) {
	return o.storage.Client().Create(ctx, request)
}
func (o *clientService) Get(ctx context.Context, request *pbo.PrimaryKeyClient) (*pbo.Client, error) {
	return o.storage.Client().Get(ctx, request)
}
func (o *clientService) GetList(ctx context.Context, request *pbo.GetListRequestClient) (*pbo.ClientListResponse, error) {
	return o.storage.Client().GetList(ctx, request)
}
func (o *clientService) Update(ctx context.Context, request *pbo.Client) (*pbo.Client, error) {
	return o.storage.Client().Update(ctx, request)
}
func (o *clientService) Patch(ctx context.Context, request *pbo.UpdatePasswordClient) (*emptypb.Empty, error) {
	return o.storage.Client().Patch(ctx, request)
}
func (o *clientService) Delete(ctx context.Context, request *pbo.PrimaryKeyClient) (*emptypb.Empty, error) {
	return o.storage.Client().Delete(ctx, request)
}

func (o *clientService) GetByCredentials(ctx context.Context, req *pbo.ClientLoginRequest) (*pbo.Client, error) {
	return o.storage.Client().GetByCredentials(ctx, req)
}
